package org.FastTrackIT;

import com.codeborne.selenide.Selenide;

public class Page {
    public Header header;
    public Footer footer;

    public void  open () {
        header = new Header();
        footer = new Footer();

        Selenide.open("https://fasttrackit-int.netlify.app/#/");
        System.out.println("Open Page. https://fasttrackit-int.netlify.app/#/ ");
    }

    public void reset () {
        header.navigateOnTheHomePage();
        footer.refresh();
    }
}
