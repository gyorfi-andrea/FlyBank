package org.FastTrackIT;

public class EmptyCartPage {
    private final String titile = "Your cart";
    private final String emptyCartSuggestion = "How about adding some products in your cart?";

    public String getEmptyCartSuggestion() {
        return emptyCartSuggestion;
    }

    public boolean isEmptyCardSuggestionVisible() {
        return !emptyCartSuggestion.isEmpty();
    }


}
