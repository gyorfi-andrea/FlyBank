package org.FastTrackIT;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ProductDetailsPage {
    private Product product;
    private final SelenideElement addToCart = $(".fa-cart-plus");
    private final SelenideElement addToWishList = $ (".row .fa-heart");
    private final SelenideElement removeFromWishlist = $ (".row .fa-heart");

    public ProductDetailsPage(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public SelenideElement getAddToCart() {
        return addToCart;
    }

    public void addToWishList() {
        if (!removeFromWishlist.isDisplayed()) {
            throw new RuntimeException("Remove from wishlist is displayed when trying to add to wishlist.");
        }
        this.addToWishList.click();
    }

    public void removeFromWishlist() {
        this.removeFromWishlist.click();
    }

    public void clickOnTheAddToCartIcon() {
        this.addToCart.click();
        System.out.println("Click on the Add to cart icon");

    }
}
