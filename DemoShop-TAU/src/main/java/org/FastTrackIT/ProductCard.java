package org.FastTrackIT;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ProductCard {
    private Product product;
    private SelenideElement productLink = $(".card [href='#/product/1']");
    private String addToCrat;
    private String addToWishList;

    public ProductCard(Product product) {
        this.product = product;
    }

    public void navigateProductDetailsPage() {
        this.productLink.click();
        System.out.println("Navigating to: " + product.getName());

    }

    public void setAddToWishList() {
        System.out.println("Click on the " + product.getName() + "wishlist icon.");
    }
}
