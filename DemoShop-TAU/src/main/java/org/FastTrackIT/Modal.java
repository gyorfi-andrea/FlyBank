package org.FastTrackIT;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private SelenideElement userFiled = $("#user-name");
    private SelenideElement passwordFiled = $("#password");
    private SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private SelenideElement errorMessage = $("p.error[data-test=\"error\"]");
    private String closeButton = "Close";
    private SelenideElement closeXButton = $("button.close");

    public void typeInUserName(String user) {
        this.userFiled.type(user);
        System.out.println("Type in " + this.userFiled + ": " + user );
    }

    public void typeInPassword(String password) {
        this.passwordFiled.type(password);
        System.out.println("Type in " +  this.passwordFiled + ": " + password);
    }

    public void clickOnTheLoginButton() {
        this.loginButton.click();
        System.out.println("Clicking on the " + loginButton);
    }

    public void closeLoginModal() {
        this.closeXButton.click();
//        System.out.println("Clicking on the " + loginButton);
    }

    public String getErrorMessage() {
        System.out.println(errorMessage.getText());
        return errorMessage.getText();
    }
}