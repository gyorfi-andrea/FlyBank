package org.FastTrackIT;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement refresh = $(".fa-undo");

    public void refresh() {
        this.refresh.click();
    }
}
