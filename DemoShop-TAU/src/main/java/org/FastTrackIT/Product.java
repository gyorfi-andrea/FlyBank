package org.FastTrackIT;

public class Product {
    private final String id;
    private final String name;
    private final String description;
    private final String price;

    public Product(String id, String name, String description, String price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getName() {
        return name;
    }
}
