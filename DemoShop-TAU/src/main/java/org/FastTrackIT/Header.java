package org.FastTrackIT;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {

    private final SelenideElement loginIcon = $(".navbar .fa-sign-in-alt");
    //private final SelenideElement logoutIcon = $(".navbar .fa-sing-out-alt");
    private final SelenideElement greetingsElement = $(".navbar-text span span");
    private String cartIcon = "Cart";
    private final SelenideElement shoppingCartBadge = $(".[href='#/cart'] .shopping_cart_badge");
    private final ElementsCollection wishlistBadge = $$(".[href='#/wishlist'] .shopping_cart_badge");
    private final SelenideElement homePageIcon = $(".navbar .brand-logo");
    private final SelenideElement searchBar = $("#input-search");
    private final SelenideElement searchButton = $("button.btn.btn-light.btn-sm");
    private final SelenideElement cardElement = $("a.card-link");
    //private... cartButton

    public void navigateOnTheHomePage() {
        this.homePageIcon.click();
    }

    public void clickOnTheLoginIcon() {
        this.loginIcon.click();
        System.out.println("Click on the " + this.loginIcon);
    }

    //clickOnCartButton()
    //cartButton.click

    public String returnElementText() {
        String elementText = cardElement.getText();
        return elementText;
    }

    public void clickOnSearchBar() {
        this.searchBar.click();
        System.out.println("Click on the " + this.searchBar);
    }

    public void clickOnSearchButton() {
        this.searchButton.click();
        System.out.println("Click on the " + this.searchButton);
    }

    public void searchByText(String text) {
        this.searchBar.type(text);
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the" + this.cartIcon);
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.getText();
    }

    public String getWishlistBadgeValue() {
        return this.wishlistBadge.texts().getFirst();
    }
    public String getWishlistBadgeClass() {
        return this.wishlistBadge.first().getAttribute("class");
    }

    public boolean isWishlistBadgeShown() {
        return this.wishlistBadge.isEmpty();
    }

    public void clickOnTheLogoutIcon() {
       // this.logoutIcon.doubleClick();
     //   System.out.println("Click on the " + this.logoutIcon);
    }

    public String getLogoutButtonClass() {
        return greetingsElement.sibling(0).$("svg").getAttribute("class");
    }


    public String getGreetingsElement() {
        return this.greetingsElement.getText();
    }

    public boolean isUserLoggedIn(String user) {
        String greetingsMessage = getGreetingsElement();
        String logoutButtonClas = getLogoutButtonClass();
        return greetingsMessage.contains("Hi") &&
                greetingsMessage.contains(user) &&
                logoutButtonClas.contains("sing-out");


    }
}
