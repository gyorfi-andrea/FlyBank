package org.FastTrackIT;

import com.codeborne.selenide.Selenide;

public class Main {
    public static void main(String[] args) {

        Page page = new Page();
        Header header = new Header();
        Modal loginModal = new Modal();
        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "fake descr", "$15.99");
        ProductCard awesomeGraniteChipsCard =  new ProductCard(awesomeGraniteChips);
        ProductDetailsPage awesomeGraniteChipsDetailPage = new ProductDetailsPage(awesomeGraniteChips);
        EmptyCartPage emptyCartPage = new EmptyCartPage();

        page.open();
        header.clickOnTheLoginIcon();
        loginModal.typeInUserName("beetle");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();
        String greetingsMassage = header.getGreetingsElement();
        boolean messageContainUser = greetingsMassage.contains("beetle");
        System.out.println("Expected result: The greetings msg shows Hi beetle! Result: " + messageContainUser);
        System.out.println("Greetings msg is: " + greetingsMassage);
        header.clickOnTheLogoutIcon();
        //-----------------------------------------
        header.clickOnTheLoginIcon();
        loginModal.typeInUserName("turtle");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();
        greetingsMassage = header.getGreetingsElement();
        messageContainUser = greetingsMassage.contains("turtle");
        System.out.println("Expected result: The greetings msg shows Hi turtle! Result: " + messageContainUser);
        System.out.println("Greetings msg is: " + greetingsMassage);
        header.clickOnTheLogoutIcon();

        //------------------------------------------
        System.out.println();
        awesomeGraniteChipsCard.navigateProductDetailsPage();
        awesomeGraniteChipsDetailPage.clickOnTheAddToCartIcon();
        System.out.println("-Expected Result: The cart icon shows 1 product added.");

        System.out.println();
        awesomeGraniteChipsCard.setAddToWishList();
        System.out.println("- Expected Result: The wishlist icon shows 1 product added.");

        System.out.println();
        header.clickOnTheCartIcon();
        boolean emptyCardSuggestionVisible = emptyCartPage.isEmptyCardSuggestionVisible();
        System.out.println("-Expected results: Empty cart suggestion is visible: " + emptyCardSuggestionVisible);
        System.out.println("-Expected results: Empty cart suggestion is valid: " + emptyCartPage.getEmptyCartSuggestion());



    }
}