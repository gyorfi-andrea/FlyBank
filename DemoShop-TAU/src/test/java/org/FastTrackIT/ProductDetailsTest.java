package org.FastTrackIT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductDetailsTest {
    static Page page = new Page();

    @BeforeAll
    public static void setup () {
        page.open();
    }

    @AfterEach
    public void tearDown() {
    page.reset();
    }

    @Test
    public void given_product_detail_page_when_clicking_on_add_to_cart_then_the_cart_icon_shows_one_product_added() {
        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "fake descr", "$15.99");
        ProductCard awesomeGraniteChipsCard =  new ProductCard(awesomeGraniteChips);
        ProductDetailsPage awesomeGraniteChipsDetailPage = new ProductDetailsPage(awesomeGraniteChips);

        awesomeGraniteChipsCard.navigateProductDetailsPage();
        awesomeGraniteChipsDetailPage.clickOnTheAddToCartIcon();

        assertEquals("1", page.header.getShoppingCartBadgeValue(), "When adding one product in cart, cart badge shows one.");

    }

    @Test
    public void given_product_detail_page_when_removing_product_from_wishlist_then_the_wishlist_icon_badge_is_not_shown() {
        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "fake descr", "$15.99");
        ProductCard awesomeGraniteChipsCard =  new ProductCard(awesomeGraniteChips);
        ProductDetailsPage awesomeGraniteChipsDetailPage = new ProductDetailsPage(awesomeGraniteChips);

        awesomeGraniteChipsCard.navigateProductDetailsPage();
        awesomeGraniteChipsDetailPage.clickOnTheAddToCartIcon();
        awesomeGraniteChipsDetailPage.removeFromWishlist();

        assertEquals("0", page.header.getShoppingCartBadgeValue(), "When adding one product in cart, cart badge shows one.");
    }

    @Test
    public void given_product_details_page_when_adding_twice_then_the_cart_icon_shows_two_products_added() {
        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "fake descr", "$15.99");
        ProductCard awesomeGraniteChipsCard =  new ProductCard(awesomeGraniteChips);
        ProductDetailsPage awesomeGraniteChipsDetailPage = new ProductDetailsPage(awesomeGraniteChips);

        awesomeGraniteChipsCard.navigateProductDetailsPage();
        awesomeGraniteChipsDetailPage.clickOnTheAddToCartIcon();

        assertEquals("2", page.header.getShoppingCartBadgeValue(), "When adding one product in cart, cart badge shows one.");
    }

    @Test
    public void given_product_detail_page_when_clicking_on_add_to_wishlist_then_the_wishlist_icon_shows_one_product_added () {
        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "fake descr", "$15.99");
        ProductCard awesomeGraniteChipsCard =  new ProductCard(awesomeGraniteChips);
        ProductDetailsPage awesomeGraniteChipsDetailPage = new ProductDetailsPage(awesomeGraniteChips);

        awesomeGraniteChipsCard.navigateProductDetailsPage();
        awesomeGraniteChipsDetailPage.addToWishList();

        assertEquals("0", page.header.getWishlistBadgeValue(), "When adding one product tot wishlist, wishlist icon shows one.");
    }

}
