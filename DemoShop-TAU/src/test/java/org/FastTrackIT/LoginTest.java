package org.FastTrackIT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LoginTest {
    static Page page = new Page();
    Modal loginModal = new Modal();
    @BeforeAll
    public static void setup () {
        page.open();
    }

    @AfterEach
    public void tearDown () {
        page.header.clickOnTheLogoutIcon();
    }

    @Test
    public void given_login_with_valid_credentials_when_user_is_beetle_then_greeting_msg_contains_beetle_user() {
        page.header.clickOnTheLoginIcon();
        loginModal.typeInUserName("beetle");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();

        assertTrue(page.header.isUserLoggedIn("beetle"), "When login with user beetle, user beetle is logged in.");

    }

    public void given_login_with_valid_credentials_when_user_is_beetle_then_greeting_msg_contains_turtle_user() {
        page.header.clickOnTheLoginIcon();
        loginModal.typeInUserName("turtle");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();
        String greetingsMessage = page.header.getGreetingsElement();
        assertEquals("Hi turtle!", greetingsMessage, "When login with user turtle, hello beetle greetings msg is displayed.");

    }

    @Test
    public void given_login_with_wrong_password_then_invalide_username_or_password_error_is_shown() {
        page.header.clickOnTheLoginIcon();
        loginModal.typeInUserName("beetle");
        loginModal.typeInPassword("pink");
        loginModal.clickOnTheLoginButton();
        String errorMassage = loginModal.getErrorMessage();
        assertEquals("Incorrect username or password!", errorMassage, "Incorrect username or password.");
    }

    @Test
    public void test_search_bar() {
        page.header.clickOnSearchBar();
        page.header.searchByText("Awesome Granite Chips");
        page.header.clickOnSearchButton();
        assertEquals(page.header.returnElementText(), "Awesome Granite Chips", "Element not found");
    }

    public void given_login_with_valid_user_but_locked_then_locked_out_error_is_displayed() {
        page.header.clickOnTheLoginIcon();
        loginModal.typeInUserName("locked");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();
        String errorMassage = loginModal.getErrorMessage();
        assertEquals("The user has been locked out.", errorMassage, "The user has been locked out.");
    }
}