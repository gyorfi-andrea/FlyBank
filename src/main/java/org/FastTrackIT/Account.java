package org.FastTrackIT;

public class Account {
    private final String iban;
    private final String currency;
    private double ballance;
    private String name;

    public Account(String iban, String currency) {
        this.iban = iban;
        this.currency = currency;
    }

    public String getIban() {
        return iban;
    }

    public String getName() {
        return name;
    }

    public double getBallance() {
        return ballance;
    }

    public String getCurrency() {
        return currency;
    }
}
