package org.FastTrackIT;

//user -> (user / password / Name / Surname / email / Phone) (/)
public class User {
    private final Credentials credentials;
    private final Account defaultAccount;
    private final String name;
    private final String surname;
    private String email;
    private String phone;


    public User(String userName, String password, String name, String surname, String loginId) {
        this.credentials = new Credentials(loginId, password);
        this.account = new Account( iban: "RO55XTRL2556655620012547" , currency: "RON");
        this.name = name;
        this.surname = surname;
        this.email = "";
        this.phone = "";
    }

    public boolean isAuthenticated(Credentials credentials) {
        return this.credentials.equals(credentials);
    }

    public Account getAccount() {
        return account;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

