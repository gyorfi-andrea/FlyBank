package org.FastTrackIT;

import java.io.InputStream;
import java.util.Scanner;

//Create user -> (user / password / Name / Surname / email / Phone) (/)
//Login user
//Display account numbers (IBAN)
//Dispaly account balance
//Create new account
//Exchange rate
//Transfer between accounts
public class Main {

    public static final int MAX_LOGIN_RETRY = 3;

    public static void main(String[] args, String loginId) {
        System.out.println(" - = FastTrack Bank = - ");
        System.out.println();
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);

        showWelcomingScreen();

        String userSelection = keyboard.nextLine();
        if (userSelection.equals("1")) {
            askUserDetails(keyboard);
        }
        
        if(userSelection.equals("2")) {
            askUserToLogin(keyboard, loginId);
        }
        System.out.print("1. View account details");
        System.out.println("2. Check account balance");
        System.out.println("3. Create new account");
        System.out.println("4. Exchange rates");
        System.out.println("5. Transfer between accounts");
        System.out.println("What would you like to do?");
        String userOption = keyboard.nextLine();
        if (userOption.equals("1")) {
    }

    private static void askUserToLogin(Scanner keyboard, String loginId) {
        User bankUser = fetchUser();
        //if bank user is authenticated with credentials....

        boolean authenticated = false;
        for (int i = 0; i < MAX_LOGIN_RETRY; i++) {
            Credentials credentials = askforCredentials(keyboard);
            authenticated = bankUser.isAuthenticated(credentials);
            if (!authenticated) {
                System.out.println("Login id or password is incorrect.");
            }
            if (authenticated) {
                break;
            }
        }
        if (authenticated) {
            System.out.println("Welcome back mr. " + bankUser.getName());
            System.out.println();
        
    }

    private static Credentials askforCredentials(Scanner keyboard) {
        System.out.print("Please enter your login id: ");
        String loginId = keyboard.nextLine();
        System.out.print("Please enter your password");
        String password = keyboard.nextLine();
        Credentials credentials = new Credentials(loginId, password);
        return credentials;
    }

    private record Credentials(String loginId, String password) {
    }

    private static void showWelcomingScreen() {
        System.out.println("Welcome to the new FastTrack Bank application");
        System.out.println();
        System.out.println("What operation would you like to do? ");
        System.out.println("1. Register new account");
        System.out.println("2. Login with existing account");
        System.out.println("your selection: ");
    }

    private static void askUserDetails(Scanner keyboard) {
        System.out.print("Please enter user: ");
        String user = keyboard.nextLine();
        System.out.print("Please enter pasaword: ");
        String password = keyboard.nextLine();

        System.out.print("Please enter Name: ");
        String name = keyboard.nextLine();
        System.out.print("Please Surname: ");
        String surname = keyboard.nextLine();

        System.out.print("Please enter email: ");
        String email = keyboard.nextLine();
        System.out.print("Please enter Phone nr: ");
        String phoneNr = keyboard.nextLine();
    }

    private static User fetchUser() {
        String user = "andrea.11";
        String password ="12345";
        String name = "Gyorfi";
        String surname ="Andrea";
        String email = "andreagyorfi23@yahoo.com";
        String phoneNr ="0748860531";
        User bankUser = new User(user, password, name, surname);
        bankUser.setEmail(email);
        bankUser.setPhone(phoneNr);
        return bankUser;
    }
}